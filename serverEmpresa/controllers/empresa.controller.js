const Empresa = require('../models/empresa');

const empresaCtrl = {};

empresaCtrl.getEmpresa = async (req, res, next) => {
    const empresa = await Empresa.find();
    res.json(empresa);
};

empresaCtrl.createEmpresa= async (req, res, next) => {
    console.log(req.body);
    const empresa = new Empresa({
        nombre: req.body.nombre,
        contraseña: req.body.contraseña,
        razonSocial: req.body.razonSocial,
        mision: req.body.mision,
        vision: req.body.vision,
        actividadEconomica: req.body.actividadEconomica,
        direccion: req.body.direccion
    });
    await empresa.save();
    res.json({status: 'empresa created'});
};

empresaCtrl.getEmp = async (req, res, next) => {
    const { nombre } = req.params;
    const empresa = await Empresa.findById(nombre);
    res.json(empresa);
};

empresaCtrl.editEmpresa= async (req, res, next) => {
    const { id } = req.params;
    const empresa = {
        nombre: req.body.nombre,
        contraseña: req.body.contraseña,
        razonSocial: req.body.razonSocial,
        mision: req.body.mision,
        vision: req.body.vision,
        actividadEconomica: req.body.actividadEconomica,
        direccion: req.body.direccion
    };
    await Empresa.findByIdAndUpdate(id, {$set: empresa}, {new: true});
    res.json({status: 'empresa Updated'});
};

empresaCtrl.deleteEmpresa= async (req, res, next) => {
    await Empresa.findByIdAndRemove(req.params.id);
    res.json({status: 'empresa Deleted'});
};

module.exports = empresaCtrl;