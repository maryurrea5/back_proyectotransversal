const mongoose = require('mongoose');
const { Schema } = mongoose;

const empresaSchema = new Schema({
    nombre: { type: String, required: true},
    contraseña: { type: String, required: true },
    razonSocial: { type: String, required: true },
    mision: { type: String, required: false },
    vision: { type: String, required: false },
    actividadEconomica: { type: String, required: false },
    direccion: { type: String, required: true },

});

module.exports = mongoose.model('Empre', empresaSchema);