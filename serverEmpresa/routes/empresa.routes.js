const express = require('express');
const router = express.Router();

const empresa = require('../controllers/empresa.controller');

router.get('/', empresa.getEmpresa);
router.post('/', empresa.createEmpresa);
router.get('/:nombre', empresa.getEmp);
router.put('/:id', empresa.editEmpresa);
router.delete('/:id', empresa.deleteEmpresa);

module.exports = router;