const sqlite3 = require('sqlite3').verbose();

const { Router } = require('express')
const router = Router();


let bd = new sqlite3.Database('tematica.db', (err) => {

    if(err)
    {
        console.log(err.message);
        
    }else {
        console.log('conectado');
        
    }

    router.get('/', (req, res)=> {
      bd.all('SELECT te.id as ID_TEMATICA,  te.titulo AS TIT_TEMATICA, te.DESCRIPCION AS DESC_TEMATICA, te.INF_ADICIONAL AS INF_TEMATICA, '+
      'ta.TITULO AS TIT_TALLER, ta.DESCRIPCION AS DESC_TALLER, ta.INF_ADICIONAL AS INF_TALLER, '+
      'p.TITULO AS TIT_PREGUNTA, p.RESPUESTA AS RES_PREGUNTA, p.INF_ADICIONAL AS INF_PREGUNTA '+
      'FROM tematica te, taller ta, pregunta p WHERE te.ID=ta.ID_TEMATICA AND TA.ID=P.ID_TALLER ',[],(er,row)=>{
 
            empleado = row;
            console.log(empleado);
            
            res.json(empleado)
        })
        
      //  setInterval(console.log('x'),500000)
    })

    router.get('/tematicabyid/:id', (req, res)=> {
        const {id} = req.params;
        bd.serialize(() => {
            
            bd.each(`SELECT te.id as ID_TEMATICA,  te.titulo AS TIT_TEMATICA, te.DESCRIPCION AS DESC_TEMATICA, te.INF_ADICIONAL AS INF_TEMATICA,
            ta.TITULO AS TIT_TALLER, ta.DESCRIPCION AS DESC_TALLER, ta.INF_ADICIONAL AS INF_TALLER,
            p.TITULO AS TIT_PREGUNTA, p.RESPUESTA AS RES_PREGUNTA, p.INF_ADICIONAL AS INF_PREGUNTA 
            FROM tematica te, taller ta, pregunta p WHERE te.ID=ta.ID_TEMATICA AND TA.ID=P.ID_TALLER AND te.ID= ${id}`, (err, rows) => {
              if (err) {
                console.error(err.message);
              }
              console.log(rows);
              
              res.json(rows)
            });
        });
    })


    router.post('/tematica', (req, res)=> {
    
        if(req.body) {
            console.log
            const {TITULO, DESCRIPCION, INF_ADICIONAL 
            } = req.body;
            const pet = req.body;
            bd.run(`INSERT INTO TEMATICA
            (TITULO, DESCRIPCION, INF_ADICIONAL)

            VALUES ('${TITULO}','${DESCRIPCION}','${INF_ADICIONAL }')`,
            (error, row) => {
                if(error) {
                    console.log(error.message);
                    res.send(error.message);
                }
                console.log('aded Tematica');
            })
            res.json(pet)
        }else {
            res.status(500).send("no guardado")
        }
    })

    router.post('/taller', (req, res)=> {
    
        if(req.body) {
            console.log
            const {TITULO, DESCRIPCION, INF_ADICIONAL, ID_DOC_EMPLEADO
            } = req.body;
            const pet = req.body;
            bd.run(`INSERT INTO TALLER
            (TITULO, DESCRIPCION , INF_ADICIONAL , ID_TEMATICA)

            VALUES ('${TITULO}','${DESCRIPCION}','${INF_ADICIONAL}','${ID_TEMATICA}')`,
            (error, row) => {
                if(error) {
                    console.log(error.message);
                    res.send(error.message);
                }
                console.log('aded Taller');
            })



            res.json(pet)
        }else {
            res.status(500).send("no guardado")
        }
    })
    router.post('/pregunta', (req, res)=> {
    
        if(req.body) {
            console.log
            const {TITULO, RESPUESTA, INF_ADICIONAL, ID_TALLER
            } = req.body;
            const pet = req.body;
            bd.run(`INSERT INTO PREGUNTA
            (TITULO, RESPUESTA , INF_ADICIONAL , ID_TALLER)

            VALUES ('${TITULO}','${RESPUESTA}','${INF_ADICIONAL}','${ID_TALLER }')`,
            (error, row) => {
                if(error) {
                    console.log(error.message);
                    res.send(error.message);
                }
                console.log('aded Taller');
            })



            res.json(pet)
        }else {
            res.status(500).send("no guardado")
        }
    })





    router.put('/updateTematica/:id', (req, res) => {
        const {id} = req.params;
        if(req.body) {
            const {name,description
                } = req.body;

            bd.run(`UPDATE tematica
            SET(TITULO, DESCRIPCION, INF_ADICIONAL)

            = ('${TITULO}','${DESCRIPCION}','${INF_ADICIONAL }')`,
            
            (error, row) => {
                if(error) {
                    console.log(error.message);
                    res.send(error.message);
                }
                console.log('edit');
            })
            res.json(req.body)


        }else {
            res.status(500).send("no guardado")
        }
    })

    router.delete('tematica/:id', (req, res) => {
 
        const { id } = req.params;
        bd.run(`DELETE FROM
        TEMATICA where ID = ${id}`,(error, row) => {
            if(error) {
                console.log(error.message);
                res.send(error.message);
            }
            console.log('delete');
        })
        
        res.json("eliminado");
    })

    router.delete('taller/:id', (req, res) => {
 
        const { id } = req.params;
        bd.run(`DELETE FROM
        TALLER where ID= ${id}`,(error, row) => {
            if(error) {
                console.log(error.message);
                res.send(error.message);
            }
            console.log('delete perfil');
        })
        
        res.json("eliminado");
    })

    router.delete('pregunta/:id', (req, res) => {
 
        const { id } = req.params;
        bd.run(`DELETE FROM
        PREGUNTA where ID= ${id}`,(error, row) => {
            if(error) {
                console.log(error.message);
                res.send(error.message);
            }
            console.log('delete perfil');
        })
        
        res.json("eliminado");
    })
})
/*

})






*/

module.exports = router;